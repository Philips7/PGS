import { PGSPage } from './app.po';

describe('pgs App', () => {
  let page: PGSPage;

  beforeEach(() => {
    page = new PGSPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
