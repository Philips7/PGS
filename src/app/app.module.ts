import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {FooterComponent} from "app/footer/footer.component";
import {LeaguesModule} from "app/leagues/leagues.module";

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    LeaguesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
