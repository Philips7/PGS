export interface NavbarInfo {
  link: string;
  title: string;
  details: string;
}
