export interface TopScorer {
  name: string;
  goals: number;
}
