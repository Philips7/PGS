import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeagueNavberComponent } from './league-navber.component';

describe('LeagueNavberComponent', () => {
  let component: LeagueNavberComponent;
  let fixture: ComponentFixture<LeagueNavberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeagueNavberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeagueNavberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
