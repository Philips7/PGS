import {Component, Input, OnInit} from '@angular/core';
import {NavbarInfo} from "app/interfaces/navbar-info";

@Component({
  selector: 'app-league-navber',
  templateUrl: './league-navber.component.html',
  styleUrls: ['./league-navber.component.css']
})
export class LeagueNavberComponent implements OnInit {

  @Input() navbarInfo: NavbarInfo;

  constructor() { }

  ngOnInit() {
  }

}
