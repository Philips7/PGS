import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LeagueNavberComponent} from "app/leagues/league-navber/league-navber.component";
import {SerieaComponent} from "app/leagues/seriea/seriea.component";
import {DataService} from "app/services/data.service";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [
    LeagueNavberComponent,
    SerieaComponent
  ],
  providers: [
    DataService
  ],
  exports: [
    SerieaComponent
  ]
})
export class LeaguesModule { }
