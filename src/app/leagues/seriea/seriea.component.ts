import {Component, OnInit} from '@angular/core';
import {DataService} from "app/services/data.service";
import {TopScorer} from "app/interfaces/top-scorer";
import {NavbarInfo} from "app/interfaces/navbar-info";

@Component({
  selector: 'app-seriea',
  templateUrl: './seriea.component.html',
  styleUrls: ['./seriea.component.css']
})
export class SerieaComponent implements OnInit {

  public topScorers: TopScorer[];
  private errorMessage: Error;
  public navbarInfo: NavbarInfo = {
    link: "seriea.jpg",
    title: "Top strzelcy Serie A",
    details: 'sezon 2016/2016'
  };

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.dataService.getSerieA().subscribe(
      response => { this.topScorers = response["data"].topscorers.slice(0, 10) },
      error => this.errorMessage = <any>Error);
  }

}
