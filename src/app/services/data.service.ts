import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, ObservableInput} from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/operator/catch";
import {TopScorer} from "app/interfaces/top-scorer";
import {ErrorObservable} from "rxjs/observable/ErrorObservable";

@Injectable()
export class DataService {

  private serieaUrl = 'https://sportsop-soccer-sports-open-data-v1.p.mashape.com/v1/leagues/serie-a/seasons/15-16/topscorers';
  private headerValue = 'kxSXmUymofmshFHhhKxWOSJpqJsJp1I3zNnjsnqKwhITAiC1zw';
  private headerName = 'X-Mashape-Key';

  constructor(private http: HttpClient) {
  }

  getSerieA(): Observable<TopScorer[]> {
    return this.http.get<TopScorer[]>(this.serieaUrl, {
      headers: new HttpHeaders()
        .set(this.headerName, this.headerValue)
    })
      .do(data => console.log('Data downloaded'))
      .catch(this.handleError);
  }

  private handleError(err: HttpErrorResponse): ObservableInput<ErrorObservable> {
    return Observable.throw(err.message);

  }

}
